<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, and ABSPATH. You can find more information by visiting
 * {@link http://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 * Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'forum2015');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', '127.0.0.1');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '@uVdKns-1Za3Vbk+)#Q,%!>|Dt{a&Zj$-+ueJ&T_Y7WN&w3;PY;&J@0N?w(aepBN');
define('SECURE_AUTH_KEY',  ')4.*E#7*fxU+b= ZDj15;<&gN9>,Jxhw<PYmesM_s-Y+>Q%|wZ+kh:~I|-OX5L%.');
define('LOGGED_IN_KEY',    '{ cH?j$cxzs,<v$!7 s[wMz|pwI;WUicIM!X*gCUkkDGvpL}/gN`LxXkMFy,<FBr');
define('NONCE_KEY',        '<q[OH;nU!?JNAT 7nqbd&g,rf=ZY;(*z,(pD{)5+,#}tfKoe-S2~D6qupN:q* [ ');
define('AUTH_SALT',        'n)!<bPZaBXj3jvzOKCVHT1zk$orgM1jzly /@)aS=<(F;f%NfE1c<Uvg64j/QrQZ');
define('SECURE_AUTH_SALT', '-22^Td!J.-3op[`oIy:RLCMB;!yth.3t7BH|8G$8@C=[~2<$s^5F;|tzP/-#K]ks');
define('LOGGED_IN_SALT',   'YP8tf&nv0JO}<-ISR+l%8`HNS[PQo=z+*;=H(aOD:jRn%[x03m[wmW~8fKO|SZ#z');
define('NONCE_SALT',       'M8t~eF9+9F<f?;`MEVcey!--7t:Qqr,+U6{EnFz-m$7v`yo-erlFuL.rOdAQ#CI1');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
