<fieldset>
	<legend>Configure the settings and ... good luck ;)</legend>
	<form action="migration.php" method="post" name="frm_migration" id="frm_migration">
		<input type="hidden" name="run_queries" value="1" />

		<p><label for="old_domain">Old domain (include subfolder if applies)</label>
		<input type="text" name="old_domain" id="old_domain" value="<?php echo $settings['olddomain'] ?>" /></p>
		<p><label for="new_domain">New domain (include subfolder if applies)</label>
		<input type="text" name="new_domain" id="new_domain" value="<?php echo $settings['newdomain'] ?>" /></p>

		<p><label for="old_path">Old path</label>
		<input type="text" name="old_path" id="old_path" value="<?php echo $settings['oldpath'] ?>" /></p>
		<p><label for="new_path">New path (usually you don't need to change it)</label>
		<input type="text" name="new_path" id="new_path" value="<?php echo $settings['newpath'] ?>" /></p>

		<p><input type="hidden" value="0" name="check_root" id="fk_check_root" />
		<input type="checkbox" value="1" name="check_root" id="check_root" <?php if($settings['checkroot']) echo 'checked="checked"'; ?> /> Check root path (usually not needed)</p>

		<p><input type="hidden" value="0" name="debug_mode" id="fk_debug_mode" />
		<input type="checkbox" value="1" name="debug_mode" id="debug_mode" checked="checked" /> Debug mode (uncheck to to run the script and changes take effect)</p>

		<?php if (!empty($form_msg)): ?>
		<p><strong><?php echo $form_msg; ?></strong></p>

		<p><label for="conn_dbname">DB name</label>
		<input type="text" name="conn_dbname" id="conn_dbname" value="<?php echo $conn['dbname'] ?>" /></p>

		<p><label for="conn_dbuser">DB user</label>
		<input type="text" name="conn_dbuser" id="conn_dbuser" value="<?php echo $conn['dbuser'] ?>" /></p>

		<p><label for="conn_password">Password</label>
		<input type="text" name="conn_password" id="conn_password" value="<?php echo $conn['dbpwd'] ?>" /></p>

		<p><label for="conn_hostname">DB hostname</label>
		<input type="text" name="conn_hostname" id="conn_hostname" value="<?php echo $conn['server']?$conn['server']:'localhost' ?>" /></p>

		<p><label for="table_prefix">Table prefix</label>
		<input type="text" name="table_prefix" id="table_prefix" value="<?php echo $settings['table_prefix'] ?>" /></p>

		<?php else: ?>
		<p><strong>Current DB connection info</strong></p>

		<p>
			<strong>DB name</strong>: <?php echo $conn['dbname'] ?><br />
			<strong>DB user</strong>: <?php echo $conn['dbuser'] ?><br />
			<strong>Password</strong>: jmmm... it's not needed to be shown<br />
			<strong>DB Hostname</strong>: <?php echo $conn['server']?$conn['server']:'localhost'; ?><br />
			<strong>Table prefix</strong>: <?php echo $settings['table_prefix'] ?><br />
		</p>
		<?php endif; ?>

		<input type="submit" value="Submit" name="btn_submit" id="btn_submit" />
	</form>
</fieldset>