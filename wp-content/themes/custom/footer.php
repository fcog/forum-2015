			<footer id="contact" class="footer container-full">

				<div class="container">

					<h2>Contact Us</h2>

					<div class="contact-info row">
						<?php $page_data = get_page( '29' ); ?>
						<div><?php echo $page_data->post_content; ?></div>
					</div>

					<div class="contact-info2 row">
						<div class="phone"><?php echo get_option( 'contact_phone' ) ?></div>
						<div class="fax"><?php echo get_option( 'contact_fax' ) ?></div>
						<a class="email" href="mailto:<?php echo get_option( 'contact_email' ) ?>"><?php echo get_option( 'contact_email' ) ?></a>
					</div>

					<div class="social-networks-footer social-networks row">
						<a href="<?php echo get_option( 'forum_twitter' ) ?>" class="twitter" title="Go to our Twitter Page">Twitter</a>
						<a href="<?php echo get_option( 'forum_facebook' ) ?>" class="facebook" title="Go to our Facebook Page">Facebook</a>
					</div>

				</div>
				
			</footer> <!-- end footer -->
		
		</div> <!-- end #container -->
				
		<!--[if lt IE 7 ]>
  			<script src="//ajax.googleapis.com/ajax/libs/chrome-frame/1.0.3/CFInstall.min.js"></script>
  			<script>window.attachEvent('onload',function(){CFInstall.check({mode:'overlay'})})</script>
		<![endif]-->
		
		<?php wp_footer(); // js scripts are inserted using this function ?>

	</body>

</html>