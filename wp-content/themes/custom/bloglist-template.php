<?php
/*
Template Name: Blog List
*/
?>

<?php get_header(); ?>

<div class="container-full newsroom">

	<div class="container">

		<div id="main-header" class="row clearfix">
			<header class="col-md-12 column">
				<h1>Blog</h1>
			</header>
		</div>

		<?php
		$category_id = get_cat_ID('home slider');
		$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
		$loop = new WP_Query( array('post_type' => 'post', 'cat' => $category_id,'posts_per_page'=>8, 'order'=> 'DESC', 'paged'=>$paged) );

		if( count($loop) > 0 ):	
		?>
			<section class="news">
			<?php
				$i=1;
			  	while ( $loop->have_posts() ) : $loop->the_post(); 
			  	?>
					<article class="row clearfix">

						<?php if (has_post_thumbnail( )): ?>

							<div class="column col-sm-2 image">
								<?php the_post_thumbnail( ); ?>
							</div>
							<div class="column col-sm-10 text">

						<?php else: ?>

							<div class="column col-sm-12 text">

						<?php endif ?>

							<header>
								<h3><a href="<?php the_permalink(); ?>"><?php the_title( ); ?></a></h3>
							</header>
							<?php the_excerpt(); ?>
						</div>
					</article>

				<?php endwhile ?>

				<?php //wp_pagenavi( array('query'=>$loop) ); ?>

			</section>

		<?php endif ?>
	</div>
</div>

<?php get_footer(); ?>