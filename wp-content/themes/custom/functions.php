<?php

function register_cpt_speaker() {
 
    $labels = array(
    	'name' => 'Speaker',
        'singular_name' => _x( 'Speaker', 'speaker' ),
        'add_new' => _x( 'Add New', 'speaker' ),
        'add_new_item' => _x( 'Add Speaker', 'speaker' ),
        'edit_item' => _x( 'Edit Speaker', 'speaker' ),
        'new_item' => _x( 'New Speaker', 'speaker' ),
        'view_item' => _x( 'View Speaker', 'speaker' ),
        'search_items' => _x( 'Search Speaker', 'speaker' ),
        'not_found' => _x( 'No speaker found', 'speaker' ),
        'not_found_in_trash' => _x( 'No speaker found in Trash', 'speaker' ),
        'parent_item_colon' => _x( 'Parent Speaker:', 'speaker' ),
        'menu_name' => _x( 'Speaker', 'speaker' ),
    );
 
    $args = array(
        'labels' => $labels,
        'hierarchical' => true,
        'description' => 'Speaker profile Posts',
        'supports' => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'revisions' ),
        'taxonomies' => array( 'category', 'post_tag' ),
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'menu_position' => 20,
         
        'show_in_nav_menus' => true,
        'publicly_queryable' => true,
        'exclude_from_search' => false,
        'has_archive' => true,
        'query_var' => true,
        'can_export' => true,
        'rewrite' => true,
        'capability_type' => 'post'
    );
 
    register_post_type( 'speaker', $args );
}

add_action( 'init', 'register_cpt_speaker' );


function register_cpt_schedule() {
 
    $labels = array(
    	'name' => 'Schedule',
        'singular_name' => _x( 'Schedule', 'schedule' ),
        'add_new' => _x( 'Add New', 'schedule' ),
        'add_new_item' => _x( 'Add Schedule', 'schedule' ),
        'edit_item' => _x( 'Edit Schedule', 'schedule' ),
        'new_item' => _x( 'New Schedule', 'schedule' ),
        'view_item' => _x( 'View Schedule', 'schedule' ),
        'search_items' => _x( 'Search Schedule', 'schedule' ),
        'not_found' => _x( 'No Schedule found', 'schedule' ),
        'not_found_in_trash' => _x( 'No Schedule found in Trash', 'schedule' ),
        'parent_item_colon' => _x( 'Parent Schedule:', 'schedule' ),
        'menu_name' => _x( 'Schedule', 'schedule' ),
    );
 
    $args = array(
        'labels' => $labels,
        'hierarchical' => true,
        'description' => 'Schedule profile Posts',
        'supports' => array( 'title', 'editor' ),
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'menu_position' => 21,
         
        'show_in_nav_menus' => true,
        'publicly_queryable' => true,
        'exclude_from_search' => false,
        'has_archive' => true,
        'query_var' => true,
        'can_export' => true,
        'rewrite' => true,
        'capability_type' => 'post'
    );
 
    register_post_type( 'schedule', $args );
}

add_action( 'init', 'register_cpt_schedule' );
/************* ADD EXCERPT TO PAGES *****************/

add_action( 'init', 'my_add_excerpts_to_pages' );
function my_add_excerpts_to_pages() {
     add_post_type_support( 'page', 'excerpt' );
}

function mytheme_setup() {
	add_theme_support( 'post-thumbnails' );
	set_post_thumbnail_size( 150, 150, true );
    add_image_size( 'speaker-box', 210, 210, true ); 
}
add_action( 'after_setup_theme', 'mytheme_setup' );

 
add_action('admin_init', 'my_general_section');  
function my_general_section() {  
    add_settings_section(  
        'my_settings_section', // Section ID 
        'Site settings', // Section Title
        'my_section_options_callback', // Callback
        'general' // What Page?  This makes the section show up on the General Settings Page
    );

    add_settings_field( // Option 1
        'forum_date', // Option ID
        'Forum Date', // Label
        'my_textbox_callback', // !important - This is where the args go!
        'general', // Page it will be displayed (General Settings)
        'my_settings_section', // Name of our section
        array( // The $args
            'forum_date' // Should match Option ID
        )  
    ); 

    add_settings_field( // Option 2
        'forum_location', // Option ID
        'Forum Location', // Label
        'my_textbox_callback', // !important - This is where the args go!
        'general', // Page it will be displayed
        'my_settings_section', // Name of our section (General Settings)
        array( // The $args
            'forum_location' // Should match Option ID
        )  
    ); 

    add_settings_field( // Option 2
        'forum_twitter', // Option ID
        'Forum Twitter', // Label
        'my_textbox_callback', // !important - This is where the args go!
        'general', // Page it will be displayed
        'my_settings_section', // Name of our section (General Settings)
        array( // The $args
            'forum_twitter' // Should match Option ID
        )  
    ); 

    add_settings_field( // Option 2
        'forum_facebook', // Option ID
        'Forum Facebook', // Label
        'my_textbox_callback', // !important - This is where the args go!
        'general', // Page it will be displayed
        'my_settings_section', // Name of our section (General Settings)
        array( // The $args
            'forum_facebook' // Should match Option ID
        )  
    ); 

    add_settings_field( // Option 2
        'contact_phone', // Option ID
        'Contact Us Phone', // Label
        'my_textbox_callback', // !important - This is where the args go!
        'general', // Page it will be displayed
        'my_settings_section', // Name of our section (General Settings)
        array( // The $args
            'contact_phone' // Should match Option ID
        )  
    ); 

    add_settings_field( // Option 2
        'contact_fax', // Option ID
        'Contact Us FAX', // Label
        'my_textbox_callback', // !important - This is where the args go!
        'general', // Page it will be displayed
        'my_settings_section', // Name of our section (General Settings)
        array( // The $args
            'contact_fax' // Should match Option ID
        )  
    ); 

    add_settings_field( // Option 2
        'contact_email', // Option ID
        'Contact Us Email', // Label
        'my_textbox_callback', // !important - This is where the args go!
        'general', // Page it will be displayed
        'my_settings_section', // Name of our section (General Settings)
        array( // The $args
            'contact_email' // Should match Option ID
        )  
    ); 

    register_setting('general','forum_date', 'esc_attr');
    register_setting('general','forum_location', 'esc_attr');
    register_setting('general','forum_twitter', 'esc_attr');
    register_setting('general','forum_facebook', 'esc_attr');
    register_setting('general','contact_phone', 'esc_attr');
    register_setting('general','contact_fax', 'esc_attr');
    register_setting('general','contact_email', 'esc_attr');
}

function my_section_options_callback() { // Section Callback
    echo '<p>U.S.-Azerbaijan Financial Forum 2015 Information</p>';  
}

function my_textbox_callback($args) {  // Textbox Callback
    $option = get_option($args[0]);
    echo '<input type="text" id="'. $args[0] .'" name="'. $args[0] .'" value="' . $option . '" />';
}