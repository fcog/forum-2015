<?php
/*
Template Name: Home
*/
?>

<?php get_header(); ?>

<main id="content" class="container-full">

	<div class="container">  

		<?php $page_data = get_page( '4' ); ?>

		<section id="about">
			
			<h3><?php echo $page_data->post_title; ?></h3>

			<?php echo get_field( 'subtitle', $page_data->ID ); ?>

			<div class="text"><?php echo $page_data->post_content; ?></div>

			<?php $page_data = get_page( '44' ); ?>

			<h2 class="vision"><?php echo $page_data->post_title; ?></h2>

			<div class="text"><?php echo $page_data->post_content; ?></div>

		</section>

	</div>

	<?php
	$loop = new WP_Query( array('post_type' => 'speaker', 'order'=> 'ASC') );

	if( count($loop) > 0 ):	
	?>

		<div id="speakers" class="container-full">
			<section class="container">

				<h3>Keynotes</h3>
				<h2>Speakers</h2>

				<div class="speakers-container row">
					<div class="flexslider">
  						<ul class="slides">
							<?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
								<li class="speaker-box col-sm-4 col-md-3">
									<div class="photo-wrap">
										<?php the_post_thumbnail( 'speaker-box' ); ?>
										<?php if ( !(empty(get_field('facebook')) && empty(get_field('twitter')) && empty(get_field('link'))) ):
										?>
											<div class="links">
												<?php if (!empty(get_field('facebook'))): ?>
													<a href="<?php echo get_field('facebook') ?>" class="facebook"></a>
												<?php endif ?>
												<?php if (!empty(get_field('twitter'))): ?>
													<a href="<?php echo get_field('twitter') ?>" class="twitter"></a>
												<?php endif ?>
												<?php if (!empty(get_field('link'))): ?>
													<a href="<?php echo get_field('link') ?>" class="link"></a>
												<?php endif ?>
											</div>
										<?php endif ?>
									</div>
									<p class="name"><?php the_title( ); ?></p>
									<p class="role"><?php echo get_field( 'role' ); ?></p>
									<?php the_excerpt(); ?>
								</li>
							<?php endwhile ?>
						</ul>
					</div>
				</div>
				
			</section>
		</div>

	<?php endif ?>	

	<?php
	$loop = new WP_Query( array('post_type' => 'schedule', 'order'=> 'ASC') );

	if( count($loop) > 0 ):	
	?>

		<section id="schedule">

			<h3>Program</h3>

			<div class="main-title2">
				<h2>Forum Schedule</h2>
				<a class="download" href="">Schedule</a>
			</div>

			<div class="schedule-container timeline">
				<?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
					<div class="schedule-box row">
						<div class="circle"></div>
						<p class="time"><?php echo get_field( 'time' ); ?></p>
						<div class="description-box">
							<div class="title-container">
								<p class="title"><?php the_title( ); ?></p>
								<p class="title2"><?php echo get_field( 'title_2' ); ?></p>
							</div>
							<?php if (!empty($post->post_content)): ?>
								<div class="toggler"></div>
								<div class="content" style="display:none"><?php the_content(); ?></div>
							<?php endif ?>
						</div>
					</div>
				<?php endwhile ?>
			</div>
			
		</section>

	<?php endif ?>	

	<section id="map">
		<div id="red-box">
			<?php $page_data = get_page( '63' ); ?>
			<div class="text"><?php echo $page_data->post_content; ?></div>
		</div>
		<div id="map-canvas"></div>
	</section>

	<?php $page_data = get_page( '10' ); ?>

	<section id="sponsors">

		<h3>Sponsors</h3>
		
		<h2><?php echo $page_data->post_title; ?></h2>

		<p><?php echo $page_data->post_content; ?></p>

		<img class="img-responsive" src="<?php echo get_stylesheet_directory_uri(); ?>/images/logos.jpg" alt="Logos">

	</section>

</main>

<script>
$(document).ready(function() {

	// One Page Nav //
	$('#menu-main-menu').onePageNav({
		currentClass: 'current-menu-item',
		easing: 'swing',
		scrollSpeed: 750,
		scrollThreshold:0.4
	});

	$('.flexslider').flexslider({
		animation: "slide",
        controlNav: false,
        animationLoop: false,
        slideshow:false,
        minItems: 4,
		maxItems: 4,
		move:4,
		itemWidth: 300,
		itemMargin: 10
	});

	function initialize() {
		var mapCanvas = document.getElementById('map-canvas');
		var myLatlng = new google.maps.LatLng(40.705567,-74.01747499999999);
	    var mapOptions = {
	        center: new google.maps.LatLng(40.705567,-74.0550374),
	      	zoom: 13,
	      	mapTypeId: google.maps.MapTypeId.ROADMAP,
	      	scrollwheel: false,
	      	panControl: false,
	      	zoomControlOptions: {
	        	style: google.maps.ZoomControlStyle.LARGE,
	        	position: google.maps.ControlPosition.RIGHT_CENTER
	    	}
	    }
	    var map = new google.maps.Map(mapCanvas, mapOptions);

	    var marker = new google.maps.Marker({
		      position: myLatlng,
		      map: map,
		      title: 'The Ritz-Carlton New York, Battery Park'
		  });
  	}
  	google.maps.event.addDomListener(window, 'load', initialize);

	$('.toggler').on('click', function(event) {
		var elem = $(this).next();
		if(elem.css('display') == 'block'){
        	elem.slideToggle( "slow" );
        	$(this).css('background-position','4px -940px');
		}
    	else{
        	elem.slideToggle( "slow" );
        	$(this).css('background-position','4px -1108px');
    	}
	});	
});

$(window).scroll(function(e){
  parallax();

  $el = $('.navbar-collapse'); 
  if ($(this).scrollTop() > 743 && $el.css('position') != 'fixed'){ 
    $el.css({'position': 'fixed', 'top': '0px'}); 
    $('#content').css('padding-top', '30px');
    $('.navbar-collapse h2').css('visibility','visible').fadeIn();
  }
  if ($(this).scrollTop() < 743 && $el.css('position') == 'fixed')
  {
    $el.css({'position': 'static', 'top': '0px'}); 
    $('#content').css('padding-top', '0');
    $('.navbar-collapse h2').css('visibility','hidden').fadeIn();
  } 
});

function parallax(){
  var scrolled = $(window).scrollTop();
  var y = (scrolled)+'px';
  $('.navbar').css('background-position','0 '+y);
}  
</script>

<?php get_footer(); ?>