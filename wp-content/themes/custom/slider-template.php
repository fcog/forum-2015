<?php
/*
Template Name: Slider
*/
?>

<?php get_header(); ?>

<div class="container-full">
			
	<?php
	$category_id = get_cat_ID('Home Slider');
	$loop = new WP_Query( array('post_type' => 'post', 'cat' => $category_id, 'order'=> 'ASC') );

	if( count($loop) > 0 ):	
	?>
		<section id="slider" class="container">

			<div class="slider featured col-xs-12 col-sm-6">
				<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
					 <!-- Slider Content (Wrapper for slides )-->
				    <div class="carousel-inner">
				    	<?php $i=0 ?>
						<?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
						    <div class="item <?php if ($i == 0): ?> active<?php endif ?>">
						    	<div class="carousel-caption">
						    		<header>
						      			<h2><a href="<?php the_permalink(); ?>" title="<?php the_title( ); ?>"><?php the_title( ); ?></a></h2>
						      		</header>
						        	<div class="section-text"><?php the_excerpt() ?></div>
						        	<a href="<?php the_permalink() ?>" title="Read more" class="read-more">more</a>
						        </div>
						    </div>
						    <?php $i++ ?>
						<?php endwhile ?>
				    </div>

					<!-- Controls -->
				    <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
				    	<span class="glyphicon glyphicon-chevron-left"></span>
				    </a>
				    <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
				    	<span class="glyphicon glyphicon-chevron-right"></span>
				    </a>
				</div>
			</div>

			<?php endwhile ?>

		</section>
	
	<?php endif ?>

	<?php wp_reset_query(); ?>

</div>


<?php get_footer(); ?>
