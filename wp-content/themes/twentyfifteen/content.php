<?php
/**
 * The default template for displaying content
 *
 * Used for both single and index/archive/search.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
?>

<article id="post-<?php the_ID(); ?>"  class="post">
	<?php
		// Post thumbnail.
		twentyfifteen_post_thumbnail();
	?>

	<div class="entry-content">
		<?php the_content() ?>

		<?php
			ob_start();
			the_content();
			$old_content = ob_get_clean();
			$new_content = strip_tags($old_content);
		?>

		<a class="tweet" href="http://www.google.com" data-text="<?php echo $new_content ?>" target="_blank">Tweet</a>
<!-- 		<a target="_blank" href="http://www.facebook.com/sharer.php?u=<?php the_permalink();?>&t=<?php the_title(); ?>">Share on Facebook</a> -->
	</div><!-- .entry-content -->

</article><!-- #post-## -->
